# Scrum Homework

## Objective

The objective of this assignment is for you to dig deeper into the details of the Scrum methodology.

## *Base Assignment*

**Everyone must complete this portion of the assignment in a *Meets Specification* fashion.**

### More Scrum Training

1. Read the [2020 Scrum Guide](https://www.scrumguides.org/scrum-guide.html) (14 pages) from [scrum.org](https://scrum.org).
2. Complete the following three assessments with a score of 75% or higher:
    1. [Scrum Open Assessment](https://www.scrum.org/open-assessments/scrum-open)
    2. [Product Owner Open Assessment](https://www.scrum.org/open-assessments/product-owner-open)
    3. [Scrum Developer Open Assessment](https://www.scrum.org/open-assessments/scrum-developer-open)

* When you complete an assessment, take a screenshot of the page showing your score.
* You may retake each assessment as many times as you wish.
  * *You may be given different questions each time you take the assessment.*

## *Intermediate Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of C or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Better Score on Scrum Open Assessments

1. Complete the three assessments from the Base Assignment with a score of 85% or higher. When you complete an assessment, take a screenshot of the page showing your score.

* When you complete an assessment, take a screenshot of the page showing your score.
* You may retake each assessment as many times as you wish.
  * *You may be given different questions each time you take the assessment.*

## *Advanced Add-On*

**Complete this portion in a *Meets Specification* fashion to apply towards base course grades of B or higher.**

* See the the Course Grade Determination table in the syllabus to see how many Intermediate Add-Ons you need to submit for a particular letter grade.
* See My Grades in Blackboard to see how many you have submitted and received an Acceptable grade on.
* You may not need to submit one for this assignment.

### Comparisons Between Kanban for Scrum

1. Read the [Kanban Guide for Scrum Teams](https://www.scrum.org/resources/kanban-guide-scrum-teams) (9 pages) from [scrum.org](https://scrum.org).
2. Complete the [Professional Scrum with Kanban Open Assessment](https://www.scrum.org/open-assessments/scrum-kanban-open)following three assessments with a score of 75% or higher.

* When you complete the assessment, take a screenshot of the page showing your score.
* You may retake the assessment as many times as you wish.
  * *You may be given different questions each time you take the assessment.*

#### Deliverables

* You must submit to the Blackboard **Scrum Homework** assignment:

  * Screenshots of the assessments you completed.
    * Name your screenshots `Base`, `Intermediate`, `Advanced`
  * Upload all screenshot files in a single submission.
  * You may resubmit the assignment as many times as you like, but ***only your last submission will be graded.***

&copy; 2024 Karl R. Wurst and Shruti Nagpal.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.

